## Model-Vendor-Path ##

Getter and Setter package for a vendor path.

This package is part of the 'Aedart\Model' namespace; visit https://bitbucket.org/aedart/model to learn more about the project.

Official sub-package website (https://bitbucket.org/aedart/model-vendor-path).

## Contents ##

[TOC]

## When to use this ##

When your component(s) need to be aware a vendor path

## How to install ##

```
#!console

composer require aedart/model-vendor-path 1.*
```

This package uses [composer](https://getcomposer.org/). If you do not know what that is or how it works, I recommend that you read a little about, before attempting to use this package.

## Quick start ##

Provided that you have an interface, e.g. for a installer, you can extend the vendor-path-aware interface;

```
#!php
<?php
use Aedart\Model\Vendor\Path\Interfaces\VendorPathAware;

interface IInstaller extends VendorPathAware {

    // ... Remaining interface implementation not shown ...
    
}

```

In your concrete implementation, you simple use the vendor-path-traits;
 
```
#!php
<?php
use Aedart\Model\Vendor\Path\Traits\VendorPathTrait;

class MyInstaller implements IInstaller {
 
    use VendorPathTrait;

    // ... Remaining implementation not shown ... 
 
}
```

### Composer's vendor directory ###

By default the `VendorPathTrait` does not contain any default vendor path. However, if you need to refer to composer's `vendor-dir`, then you can use the `ComposerVendorPathTrait`, which guesses the full path to the vendor directory,
based upon the filesystem location of Composer's `ClassLoader` file.

```
#!php
<?php
use Aedart\Model\Vendor\Path\Traits\ComposerVendorPathTrait;

class MyInstaller implements IInstaller {
 
    use ComposerVendorPathTrait;

    // ... Remaining implementation not shown ... 
 
}
```

## License ##

[BSD-3-Clause](http://spdx.org/licenses/BSD-3-Clause), Read the LICENSE file included in this package