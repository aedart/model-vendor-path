<?php  namespace Aedart\Model\Vendor\Path\Interfaces;

use Aedart\Model\Vendor\Path\Exceptions\InvalidVendorPathException;

/**
 * Interface Vendor Path Aware
 *
 * Components, classes or objects that implements this interface, promise that a vendor path can be specified and retrieved.
 * Furthermore, depending upon implementation, a default vendor path might be returned, if no path has been set prior to obtaining it.
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Model\Vendor\Path\Interfaces
 */
interface VendorPathAware {

    /**
     * Set the vendor path
     *
     * @param string $directoryPath The vendor path (directory path)
     *
     * @return void
     *
     * @throws InvalidVendorPathException If given path is not a valid directory
     */
    public function setVendorPath($directoryPath);

    /**
     * Get the vendor path
     *
     * If no vendor path has been set, this method sets and
     * returns a default vendor path, if any is available
     *
     * @see getDefaultVendorPath()
     *
     * @return string|null The vendor path (directory path) or null if none has been set
     */
    public function getVendorPath();

    /**
     * Get a default vendor path, if any is available
     *
     * @return string|null A default vendor path (directory path), or null if none is available
     */
    public function getDefaultVendorPath();

    /**
     * Check if a vendor path has been set
     *
     * @return bool True if a vendor path has been set, false if not
     */
    public function hasVendorPath();

    /**
     * Check if a default vendor path is available
     *
     * @return bool True if a default vendor path is available, false if not
     */
    public function hasDefaultVendorPath();

    /**
     * Check if the given vendor path is valid; if it is a directory
     *
     * @param mixed $directoryPath The directory path to be validated
     *
     * @return bool True if the given directory is valid, false if not
     */
    public function isVendorPathValid($directoryPath);
}