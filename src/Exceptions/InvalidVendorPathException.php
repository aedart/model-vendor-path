<?php  namespace Aedart\Model\Vendor\Path\Exceptions; 

/**
 * Class Invalid Vendor Path Exception
 *
 * Throw this exception when an invalid vendor path has been given
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Model\Vendor\Path\Exceptions
 */
class InvalidVendorPathException extends \InvalidArgumentException{

}