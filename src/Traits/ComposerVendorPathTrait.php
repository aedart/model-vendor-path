<?php  namespace Aedart\Model\Vendor\Path\Traits; 

use Aedart\Model\Vendor\Path\Traits\VendorPathTrait;
use Composer\Autoload\ClassLoader;
use \ReflectionClass;

/**
 * Trait Composer Vendor Path
 *
 * Specialised trait which (by default) returns the full path to the
 * current composer project's 'vendor' folder. it does so by guessing the
 * location of the vendor directory, based upon the location of composer's
 * ClassLoader file
 *
 * @see \Composer\Autoload\ClassLoader
 * @see \Aedart\Model\Vendor\Path\Traits\VendorPathTrait
 * @see \Aedart\Model\Vendor\Path\Interfaces\VendorPathAware
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Model\Vendor\Path\Traits
 */
trait ComposerVendorPathTrait {

    use VendorPathTrait;

    /**
     * Get a default vendor path, if any is available
     *
     * @return string|null A default vendor path (directory path), or null if none is available
     */
    public function getDefaultVendorPath() {
        // In order to know the exact location of the 'vendor directory', we can
        // have a number of options;
        // a) Assume this trait / package is already location inside the vendor folder and move upwards (confusing to test)
        // b) Use composer and attempt to read root package's configuration (safest, but also hardest to code)
        // c) We assume that composer was used to install this package, meaning that somewhere inside composer, there
        //      is a 'ClassLoader', which should also be located inside the 'vendor' folder. Thus, we use its nested
        //      position to navigate out into the 'vendor' folder
        $reflection = new ReflectionClass(new ClassLoader());
        $vendorFolder = realpath(dirname(dirname($reflection->getFileName())));
        return $vendorFolder;
    }
}