<?php  namespace Aedart\Model\Vendor\Path\Traits; 

use Aedart\Model\Vendor\Path\Exceptions\InvalidVendorPathException;
use Aedart\Validate\Filesystem\DirectoryValidator;

/**
 * Trait Vendor Path
 *
 * @see \Aedart\Model\Vendor\Path\Interfaces\VendorPathAware
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Model\Vendor\Path\Traits
 */
trait VendorPathTrait {

    /**
     * The vendor path
     *
     * @var string|null
     */
    protected $vendorPath = null;

    /**
     * Set the vendor path
     *
     * @param string $directoryPath The vendor path (directory path)
     *
     * @return void
     *
     * @throws InvalidVendorPathException If given path is not a valid directory
     */
    public function setVendorPath($directoryPath){
        if(!$this->isVendorPathValid($directoryPath)){
            throw new InvalidVendorPathException(sprintf('%s is not a valid directory', var_export($directoryPath, true)));
        }
        $this->vendorPath = $directoryPath;
    }

    /**
     * Get the vendor path
     *
     * If no vendor path has been set, this method sets and
     * returns a default vendor path, if any is available
     *
     * @see getDefaultVendorPath()
     *
     * @return string|null The vendor path (directory path) or null if none has been set
     */
    public function getVendorPath(){
        if(!$this->hasVendorPath() && $this->hasDefaultVendorPath()){
            $this->setVendorPath($this->getDefaultVendorPath());
        }
        return $this->vendorPath;
    }

    /**
     * Get a default vendor path, if any is available
     *
     * @return string|null A default vendor path (directory path), or null if none is available
     */
    public function getDefaultVendorPath(){
        return null;
    }

    /**
     * Check if a vendor path has been set
     *
     * @return bool True if a vendor path has been set, false if not
     */
    public function hasVendorPath(){
        if(!is_null($this->vendorPath)){
            return true;
        }
        return false;
    }

    /**
     * Check if a default vendor path is available
     *
     * @return bool True if a default vendor path is available, false if not
     */
    public function hasDefaultVendorPath(){
        if(!is_null($this->getDefaultVendorPath())){
            return true;
        }
        return false;
    }

    /**
     * Check if the given vendor path is valid; if it is a directory
     *
     * @param mixed $directoryPath The directory path to be validated
     *
     * @return bool True if the given directory is valid, false if not
     */
    public function isVendorPathValid($directoryPath){
        return DirectoryValidator::isValid($directoryPath);
    }

}