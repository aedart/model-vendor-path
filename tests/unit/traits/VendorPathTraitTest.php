<?php

use Aedart\Model\Vendor\Path\Interfaces\VendorPathAware;
use Aedart\Model\Vendor\Path\Traits\VendorPathTrait;
use Faker\Factory as FakerFactory;
use Codeception\Configuration;

/**
 * Class VendorPathTraitTest
 *
 * @coversDefaultClass Aedart\Model\Vendor\Path\Traits\VendorPathTrait
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class VendorPathTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * Faker
     *
     * @var \Faker\Generator
     */
    protected $faker = null;

    protected function _before()
    {
        $this->faker = FakerFactory::create();
    }

    protected function _after()
    {
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * @return PHPUnit_Framework_MockObject_MockObject|Aedart\Model\Vendor\Path\Interfaces\VendorPathAware
     */
    protected function getTraitMock(){
        $m = $this->getMockForTrait('Aedart\Model\Vendor\Path\Traits\VendorPathTrait');
        return $m;
    }

    /**
     * Get a dummy class
     *
     * @return Aedart\Model\Vendor\Path\Interfaces\VendorPathAware
     */
    protected function getDummyClass(){
        return new DummyVendorPathClass();
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::hasDefaultVendorPath
     * @covers ::getDefaultVendorPath
     */
    public function getNullAsDefaultVendorPath(){
        $trait = $this->getTraitMock();
        $this->assertFalse($trait->hasDefaultVendorPath());
        $this->assertNull($trait->getDefaultVendorPath());
    }

    /**
     * @test
     * @covers ::getVendorPath
     * @covers ::hasVendorPath
     * @covers ::hasDefaultVendorPath
     * @covers ::setVendorPath
     * @covers ::getDefaultVendorPath
     * @covers ::isVendorPathValid
     */
    public function getCustomDefaultVendorPath(){
        $dummy = $this->getDummyClass();
        $this->assertSame(Configuration::helpersDir(), $dummy->getVendorPath());
    }

    /**
     * @test
     * @covers ::getVendorPath
     * @covers ::hasVendorPath
     * @covers ::setVendorPath
     * @covers ::isVendorPathValid
     */
    public function setAndGetVendorPath(){
        $trait = $this->getTraitMock();

        $path = Configuration::dataDir();

        $trait->setVendorPath($path);

        $this->assertSame($path, $trait->getVendorPath());
    }

    /**
     * @test
     * @covers ::getVendorPath
     * @covers ::hasVendorPath
     * @covers ::setVendorPath
     * @covers ::isVendorPathValid
     *
     * @expectedException \Aedart\Model\Vendor\Path\Exceptions\InvalidVendorPathException
     */
    public function attemptSetInvalidVendorPath(){
        $trait = $this->getTraitMock();

        $path = $this->faker->address;

        $trait->setVendorPath($path);
    }
}

class DummyVendorPathClass implements  VendorPathAware {
    use VendorPathTrait;

    public function getDefaultVendorPath() {
        return Configuration::helpersDir();
    }
}