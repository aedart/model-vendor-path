<?php

use Aedart\Model\Vendor\Path\Interfaces\VendorPathAware;
use Aedart\Model\Vendor\Path\Traits\ComposerVendorPathTrait;

/**
 * Class ComposerVendorPathTraitTest
 *
 * @coversDefaultClass Aedart\Model\Vendor\Path\Traits\ComposerVendorPathTrait
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class ComposerVendorPathTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * @return PHPUnit_Framework_MockObject_MockObject|Aedart\Model\Vendor\Path\Interfaces\VendorPathAware
     */
    protected function getTraitMock(){
        $m = $this->getMockForTrait('Aedart\Model\Vendor\Path\Traits\ComposerVendorPathTrait');
        return $m;
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::getDefaultVendorPath
     */
    public function getDefaultVendorPath(){
        // Make an assumption about where the vendor folder is
        $assumedVendorFolder = getcwd() . '/vendor';

        $trait = $this->getTraitMock();
        $this->assertSame($assumedVendorFolder, $trait->getDefaultVendorPath());
    }
}